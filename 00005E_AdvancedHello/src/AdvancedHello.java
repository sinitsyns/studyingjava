//ипмортировали библиотеку ProJava
import static pro.java.util.Print.*;
public class AdvancedHello {

	public static void main(String[] args) {
		// Вывод на консоль с помощью библиотеки ProJava.jar
		print("Hello "); //вывели Hello и остались на той же строке
		println("World!"); //вывели World и перешли на другую строку
		println(); //вывели пустую строку и перешли на новую
		printf("Hello %s!", "World");

	}

}
