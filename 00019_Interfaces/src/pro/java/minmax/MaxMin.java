package pro.java.minmax;

public class MaxMin implements Min, Max {


	@Override
	public int getMax(int i, int j) {
		if (i > j) return i;
		return j;
	}

	@Override
	public int getMin(int i, int j) {
		if (i > j) return j;
		return i;
	}

}
