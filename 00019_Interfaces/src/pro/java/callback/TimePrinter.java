package pro.java.callback;

import static pro.java.util.Print.println;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class TimePrinter implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		Date now = new Date();
		println("Текущее время: " + now);

	}

}
