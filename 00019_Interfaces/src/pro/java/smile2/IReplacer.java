package pro.java.smile2;

public interface IReplacer {
	
	String replace(IReader reader, String from, String to);

}
