package pro.java.smile2;

public class DefaultReplacer implements IReplacer {

	@Override
	public String replace(IReader reader, String from, String to) {
		return reader.read().replace(from, to);
	}

}
