package pro.java.staticm;

interface MyInt {

	static void prt1() {
		System.out.println("PRT1 in MyInt");
	}

	void prt2();
}

class Prt implements MyInt {
	@Override
	public void prt2() {
		System.out.println("PRT2 in Prt");
	}
}

public class Interf01 implements MyInt{

	public static void main(String[] args) {

		// вызов статического метода на интерфейсе
		MyInt.prt1(); 
		
		Prt prt = new Prt();
		prt.prt2();
		// prt.prt1(); // ошибка
		
		Interf01 ifs = new Interf01();
		ifs.prt2();

	}

	@Override
	public void prt2() {
		System.out.println("PRT2 in main()");
	}

}
