package pro.java.intr01;

public interface Sound {
	
	default void getSound() {
		System.out.println("----");
	}
	
	String getType();

}
