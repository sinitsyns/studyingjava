package pro.java.inner03;

import static pro.java.util.Print.*;

public class Outer {

	private String outstr = "Outer";
	private String str = "strOuter00";
	protected String proOut = "proOut";

	class Inner01 extends Outer {

		private String strInn01 = "strInn01";
		protected String str = "strInner01";
		protected String prostr = "proStrInn01";

		String getOutStr() { return outstr;	}
		String getStrInn01() { return strInn01;	}

		void printStr() {
			println("Inner01>" + str);
			println("Inner01>" + Inner01.super.str);
		}

		class Inner02 extends Outer.Inner01 {
			public Inner02() { this(new Outer().new Inner01()); }
			public Inner02(Outer.Inner01 o) { o.super(); }
			private String str = "strInner02";
			String getOutStr() { return outstr; }

			void printStr() {
				println("Inner02>" + str);
				println("Inner02>" + super.str);
			}
		}
	}

	void printStr() { println(str);	}

}

class External extends Outer.Inner01 {
	public External() { this(new Outer().new Inner01()); }
	public External(Outer.Inner01 o) { o.super(); }
	private String str = "strExternal";

	void printStr() {
		println("External>" + str);
		println("External>" + super.str);
		println("External>" + prostr);
		println("External>" + proOut);
		super.printStr();
		println("External>" + getOutStr());
		println("External>" + getStrInn01());
	}
}
