package pro.java.inner03;

import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {
		// Примеры наследования внутренних классов и операторов new и super
		Outer out = new Outer();
		Outer.Inner01 in01 = out.new Inner01();
		Outer.Inner01.Inner02 in02 = in01.new Inner02(in01);
		Outer.Inner01.Inner02 in021 = new Outer().new Inner01().new Inner02();
		println(in01.getOutStr());
		println(in02.getOutStr());
		println(in02.getStrInn01());
		in01.printStr();
		in02.printStr();
		println("---------");
		in021.printStr();
		println("*********");
		External ex = new External();
		ex.printStr();
		
	}

}
