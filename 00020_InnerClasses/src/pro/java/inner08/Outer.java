package pro.java.inner08;

import static pro.java.util.Print.*;

public class Outer {
	private String outerStr = "Outer";
	Outer() {};
	Outer(String str) { outerStr = str; }
	
	void printInnerStr(Outer.Inner inn){
// строка ниже выдаст ошибку компиялции		
//		println("Print from Outer innerStr = " + Inner.innerStr);
		println("Print from Outer innerStr = " + inn.innerStr);
	}
	
	class Inner {
		Inner() {};
		Inner(String str) { innerStr = str; };
		private String innerStr = "Inner";
		void printOterStr(){
			println("Print from Inner outerStr = " + outerStr);
		}
	}
}
