package pro.java.nestext;

import static pro.java.util.Print.println;


public class Main {

	public static void main(String[] args) {
		
		Outer.Nested ou = new Outer.Nested();
		println(ou.getName());
		
		Outer n = new Outer();
		println(n.getName());
		
		Outer.Nested.Ext1 ext1 = new Outer.Nested.Ext1();
		println(ext1.getName());
		
		Ext2 ext2 = new Ext2();
		println(ext2.getName());

	}

}
