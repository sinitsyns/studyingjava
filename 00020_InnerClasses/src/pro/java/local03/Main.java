package pro.java.local03;

import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {
		GetLocalClass lc = new External().getLocal("test");
		println(lc.getLocalStr());
		println(lc.getStr());
	}

}
