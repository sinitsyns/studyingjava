package pro.java.anonym07;

import static pro.java.util.Print.*;


// это то что выпускает наша фабрика - продукт
interface Service {
	void method1();
	void method2();
	void ServiceName();
}

// это фабрика выпускающая продукт - Service
interface ServiceFactory {
	Service getService();
}

// это первая имплементация продукта - сервиса
class Implementation1 implements Service {
	String serviceName = "NoNameService1";
	Implementation1() {}
	Implementation1(String service) {
		serviceName = service;
	}
	public void ServiceName(){
		println(serviceName);
	}
	public void method1() { println("I1 M1"); }
	public void method2() { println("I1 M2"); }
	// первая имплементация фабрики 
	public ServiceFactory factory = new ServiceFactory() {
		public Service getService() {
			return new Implementation1(serviceName);
		}
	};
}

//это вторая имплементация продукта - сервиса
class Implementation2 implements Service {
	String serviceName = "NoNameService2";
	Implementation2() {}
	Implementation2(String service) {
		serviceName = service;
	}
	public void ServiceName(){
		println(serviceName);
	}
	public void method1() { println("I2 M1"); }
	public void method2() { println("I2 M2"); }
	// вторая имплементация фабрики 
	public ServiceFactory factory = new ServiceFactory() {
		public Service getService() {
			return new Implementation2(serviceName);
		}
	};
}

public class Factories {
	static void serviceConsumer(ServiceFactory fact){
		Service s = fact.getService();
		s.method1();
		s.method2();
		s.ServiceName();
	}

	public static void main(String[] args) {
		serviceConsumer(new Implementation1("Service1").factory);
		println();
		serviceConsumer(new Implementation2().factory);
	}
}
