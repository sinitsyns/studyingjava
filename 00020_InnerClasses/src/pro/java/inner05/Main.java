package pro.java.inner05;

import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {
		Outer o = new Outer();
		Outer.Inner i = o.new Inner();
		println("o.getThis -> " + o.getThis());
		println("o.getStr  -> " + o.getStr());
		println("i.getThis -> " + i.getThis());
		println("i.getStr  -> " + i.getStr());
		Outer.SuperInner s = i.new SuperInner(i);
		println("s.getThis -> " + s.getThis());
		println("s.getStr  -> " + s.getStr());

	}

}
