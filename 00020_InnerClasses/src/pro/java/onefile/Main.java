package pro.java.onefile;

//пример нескольких классов верхнего уровня в одном .java файле

public class Main {

	public static void main(String[] args) {
		Example e = new Example();
		e.testPrint();
	}

}

// класс Example верхенего уровня (не вложенный)
class Example {
	void testPrint() {
		System.out.println("TestPrint");
	}
}

