package pro.java.anonym05;

import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {
		
		Account father = new Account("16108-1108", "BIG FATHER");
		println("father.owener -> " + father.getOwner());
		println("father.number -> " + father.getNumber());
		println("father.amount -> " + father.getAmount());
		Account.Card mother = father.new Card("mother108", "BIG MOTHER");
		println("mother.onwner -> " + mother.getOwner());
		println("mother.number -> " + mother.getNumber());
		println("mother.amount -> " + mother.getAmount());
		println("mother.withdrow " + mother.withdraw(18));
		println("mother.amount -> " + mother.getAmount());
		println("father.amount -> " + father.getAmount());
		mother.toDeposit(8);
		println("mother toDeposit(8)");
		println("mother.amount -> " + mother.getAmount());
		println("father.amount -> " + father.getAmount());
		father.toDeposit(10);
		println("father toDeposit(10)");
		println("mother.amount -> " + mother.getAmount());
		println("father.amount -> " + father.getAmount());

	}

}
