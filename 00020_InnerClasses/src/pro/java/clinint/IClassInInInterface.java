package pro.java.clinint;

public interface IClassInInInterface {
	void printHello();
	
	class Print implements IClassInInInterface {

		@Override
		public void printHello() {
			System.out.println("Hello from Interface!");
		}
		
		public static void main(String[] args){
			new Print().printHello();
		}
		
	}

}
