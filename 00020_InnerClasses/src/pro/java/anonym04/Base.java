package pro.java.anonym04;

import static pro.java.util.Print.println;

public class Base {
	private String str = "Base";
	Base(String str) {
		println("Base constructor, str= " + str);
		println("Base constructor, this.str= " + this.str);
	};
	Object getThis() { return this; }
	String getStr() { return str; }
	Base getAnonBase(String str) {
		return new Base(str) {
			{
				println("Инициализатор анонимного класса");
				println("str = " + str);
				println("Base.this.str = " + Base.this.str + '\n');
			}
		};
	}
}

class External extends Base {
	External () { this("Default External");};
	External(String str) { super(str); }
	private String str = "External";
	Base getAnonBase(String str) {
		return new Base(str) {
			{
				println("Инициализатор анонимного класса");
				println("str = " + str);
				println("External.this.str = " + External.this.str);
				println("getStr() = " + getStr() + '\n'); 
			}
		};
	}
}
