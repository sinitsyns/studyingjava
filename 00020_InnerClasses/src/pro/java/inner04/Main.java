package pro.java.inner04;

import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {

		Outer out = new Outer();
		println("out.this - > " + out.getThis());
		Outer.Inner1 in1 = out.new Inner1();
		println("in1.this - > " + in1.getThis());
		println("in1.OuterThis - > " + in1.getOuterThis());
		println("in1.getStrInner1 - > " + in1.getStrInner1());
		println("in1.getStrOuter  - > " + in1.getStrOuter());

	}

}
