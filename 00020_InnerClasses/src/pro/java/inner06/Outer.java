package pro.java.inner06;

import static pro.java.util.Print.*;

public class Outer {
	String str = "Outer";
	
	class Inner extends External {
		String str = "Inner";
		void printStr() { 
			println(str);
			println(super.str);
			println(Outer.this.str);
		}
	}
}

class External {
	String str = "External";
}
