package pro.java.inner06;

public class Main {

	public static void main(String[] args) {
		
		Outer o = new Outer();
		Outer.Inner i = o.new Inner();
		i.printStr();

	}

}
