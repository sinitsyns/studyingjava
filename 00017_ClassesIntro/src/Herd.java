import static pro.java.util.Print.*;

public class Herd {

	public static void main(String[] args) {
		// примеры static полей и методов

		Cow cow1 = new Cow();
		println("Cow.count = " + Cow.count);
		Cow cow2 = new Cow("Буренка");
		println("Cow.getCount() = " + Cow.getCount());
		println();
		// так делать не рекомендуется
		println("cow1.count = " + cow1.count);
		println("cow2.getCount() = " + cow2.getCount());
		
	}

}
