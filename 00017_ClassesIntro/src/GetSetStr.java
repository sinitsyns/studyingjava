import static pro.java.util.Print.*;

public class GetSetStr {
	String string;
	
	GetSetStr(){
		string = "default";
	}
	
	GetSetStr(String str){
		string = str;
	}

	void setStr(String str) {
		string = str;
	}

	void printStr() {
		println(string);
	}
	
	String getStr(){
		return string;
	}
}
