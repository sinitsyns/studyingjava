import static pro.java.util.Print.*;

public class Classes009 {
	
	public static void main(String[] args) {
		
		println("В начале метода main()");
		println("Classes010.str4 = " + Classes010.str4);
		println("Classes010.str6 = " + Classes010.str6);
		
		Classes010 c10 = new Classes010();
		println("c10.str1 = " + c10.str1);
		println("c10.str2 = " + c10.str2);
		println("c10.str5 = " + c10.str5);
		
	}

}
