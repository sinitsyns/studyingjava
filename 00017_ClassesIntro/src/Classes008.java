import static pro.java.util.Print.*;

public class Classes008 {
	
	final static String FIELD = "FIELD";

	static String zero(int i) {
		final String str;
		if (i == 0)
			str = "НОЛЬ";
		else
			str = "НЕ НОЛЬ";
		return str;
	}

	public static void main(String[] args) {

		println(zero(0));
		println(zero(1));
		println(FIELD);

	}

}
