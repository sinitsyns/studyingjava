import static pro.java.util.Print.*;

public class Classes002 {

	static Box fieldBox; // поле класса (статическое)

	public static void main(String[] args) {
		// подробный пример создания экземпляра класса

		println("fieldBox = " + fieldBox);

		Box mybox; // объявление ссылки на объект

		// println("mybox = " + mybox); // ошибка компиляции

		mybox = null;
		println("mybox = " + mybox);

		mybox = new Box(); // создание в памяти экземпляра объекта Box
		println("mybox = " + mybox);
		println("mybox.height = " + mybox.height);
		
		printLine();
		println();
		
		Box b1 = new Box();
		b1.label = "box1";
		println("box1.label = " + b1.label);
		Box b2 = b1; // b1 и b2 ссылаются на один объект
		b2.label = "box2";
		println("box1.label = " + b1.label);
		b2 = new Box(); // теперь b2 ссылается на другой объект
		b1.label = "box1";
		b2.label = "box2";
		printLine();
		println();
		println("box1.label = " + b1.label);
		println("box2.label = " + b2.label);

	}

}
