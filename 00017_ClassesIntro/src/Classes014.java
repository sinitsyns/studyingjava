import static pro.java.util.Print.*;

public class Classes014 {

	public static void main(String[] args) {
		// Пример паттерна Builder
		Contact contact1 = new ContactBuilder("Пуркин")
				.phone("123")
				.name("Вася")
				.address("Уфа")
				.email("v@p.ru")
				.bulid();
		println("contact1 name = " + contact1.getName());
		println("contact1 surname = " + contact1.getSurname());
		println("contact1 email = " + contact1.getEmail());
		println("contact1 phone = " + contact1.getPhone());
		println("contact1 adress = " + contact1.getAddress());
		println("---------------");
		Contact contact2 = new ContactBuilder("Пупкин")
				.phone("456")
				.name("Вася")
				.bulid();
		println("contact2 name = " + contact2.getName());
		println("contact2 surname = " + contact2.getSurname());
		println("contact2 email = " + contact2.getEmail());
		println("contact2 phone = " + contact2.getPhone());
		println("contact2 adress = " + contact2.getAddress());
		

	}

}
