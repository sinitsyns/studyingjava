import static pro.java.util.Print.*;

public class Classes010 {
	// примеры инициализационных блоков

	String str1, str2, str5;
	String str3 = "Привет мир!";

	static String str4, str6;

	// инициализационный блок
	{
		println("В инициализационном блоке");
		str1 = "Hello World!";
	}

	// статический инициализационный блок
	static {
		println("В статическом инициализационном блоке 1");
		str4 = "STATIC";
	}
	
	static {
		println("В статическом инициализационном блоке 2");
		str6 = "static";
	}
	
	Classes010(){
		println("В конструкторе по умолчанию");
		str2 = "STRING";
	}

}
