import static pro.java.util.Print.*;

public class Dim {

	String[] strArray;

	Dim(String... strings) {
		strArray = strings;
	}

	void setIndex(int a, String str) {
		strArray[a] = str;
	}
	
	void printIndex(int a){
		print(strArray[a]);
	}
	
	String[] getStrArray(){
		return strArray;
	}

}
