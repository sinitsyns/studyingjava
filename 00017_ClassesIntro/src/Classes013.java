import static pro.java.util.Print.*;
import static pro.java.util.Sys.iHash;

public class Classes013 {

	public static void main(String[] args) {
		println("------- str1 & str2 --------");
		Str str1 = new Str("Str1");
		str1.printStrHash();
		String str2 = "Str1";
		println("хэш объекта  str2 = " + iHash(str2) + "  строка = " + str2);

		println("\n------- str3 & str4 --------");
		Str str3 = new Str(new String("Str1"));
		str3.printStrHash();
		String str4 = str3.getStr();
		println("хэш объекта  str4 = " + iHash(str4) + "  строка = " + str4);

		println("\n------- str4 --------");
		str4 = str4.intern();
		println("хэш объекта  str4 = " + iHash(str4) + "  строка = " + str4);

		println("\n------- Arrays --------");
		Dim dim1 = new Dim("один", "два", "три");
		print("Массив в классе Dim: ");
		for (String str : dim1.getStrArray()) {
			print(" " + str);
		}
		println();
		String[] strArray = dim1.getStrArray();
		for (int i = 0; i < strArray.length; ++i) {
			strArray[i] = String.valueOf(i + 1);
		}
		print("Массив в классе Dim: ");
		for (String str : dim1.getStrArray()) {
			print(" " + str);
		}

		dim1.setIndex(0, "one");
		dim1.setIndex(1, "two");
		dim1.setIndex(2, "three");
		println();
		print("Массив в классе strArray: ");
		for (String str : strArray) {
			print(" " + str);
		}

	}

}
