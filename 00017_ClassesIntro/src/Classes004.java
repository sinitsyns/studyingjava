import static pro.java.util.Print.*;

public class Classes004 {

	public static void main(String[] args) {

		Box box1 = new Box();
		println("box1.depth = " + box1.depth);
		Box box2 = new Box(10);
		println("box2.depth = " + box2.depth);
		Box box3 = new Box(10, 20, 30);
		println("box3.depth = " + box3.depth);

	}

}
