import static pro.java.util.Print.*;

public class Classes001 {

	public static void main(String[] args) {

		Box box1 = new Box(10, 20, 30);
		Box box2 = new Box(15, 15, 15);
		
		println("box 1 sizes");
		box1.printSizes();
		println("box1 volume = " + box1.getVolume());
		print("box 1 print volume ");
		box1.printVolume();

		println("\n\nbox 2 sizes");
		box2.printSizes();
		println("box2 volume = " + box2.getVolume());
		print("box 2 print volume ");
		box2.printVolume();

	}

}
