import java.math.BigDecimal;
import static pro.java.util.Print.*;

public class BigDecimalClass {

	public static void main(String[] args) {
		// Примеры работы с BigDecimal

		// пример сути проблемы с точностью представления double
		double d1 = 2;
		double d2 = 1.1;
		println("d1 = " + d1 + "   d2 = " + d2);
		println("d1-d2 = " + (d1 - d2));

		// как это решается с использованием BigDecimal
		BigDecimal bd1 = BigDecimal.valueOf(2);
		BigDecimal bd2 = BigDecimal.valueOf(1.1);
		println("bd1 = " + bd1 + "   bd2 = " + bd2);
		println("bd1-bd2 = " + (bd1.subtract(bd2)));

		double d3 = 10;
		double d4 = 0.0825;
		println("d3*d4 = " + (d3 * d4));

		// лучше создавать объекты BigDecimal из строки
		// или при помощи метда valueOf()
		BigDecimal bd3 = new BigDecimal(0.3);
		println("bd3 = " + bd3);
		bd3 = new BigDecimal("0.3");
		println("bd3 = " + bd3);
		bd3 = BigDecimal.valueOf(0.3);
		println("bd3 = " + bd3);

		// примеры округления
		bd3 = new BigDecimal(0.3);
		// bd3.setScale(1); //выдаст ошибку
		println("ROUND_CEILING bd3 = " + bd3.setScale(1, BigDecimal.ROUND_CEILING));

		// ROUND_CEILING: В большую сторону
		println();
		println("ROUND_CEILING: В большую сторону");
		BigDecimal bdr = new BigDecimal("0.333");
		print("bdr =  " + bdr);
		println("   ROUND_CEILING bdr =  " + bdr.setScale(2, BigDecimal.ROUND_CEILING));
		bdr = new BigDecimal("-0.333");
		print("bdr = " + bdr);
		println("   ROUND_CEILING bdr = " + bdr.setScale(2, BigDecimal.ROUND_CEILING));

		// ROUND_DOWN: Округление в меньшую сторону по модулю
		println();
		println("ROUND_DOWN: Округление в меньшую сторону по модулю");
		bdr = new BigDecimal("0.333");
		print("bdr =  " + bdr);
		println("   ROUND_DOWN bdr =  " + bdr.setScale(2, BigDecimal.ROUND_DOWN));
		bdr = new BigDecimal("-0.333");
		print("bdr = " + bdr);
		println("   ROUND_DOWN bdr = " + bdr.setScale(2, BigDecimal.ROUND_DOWN));

		// ROUND_UP: Округление в большую сторону по модулю
		println();
		println("ROUND_UP: Округление в большую сторону по модулю");
		bdr = new BigDecimal("0.333");
		print("bdr =  " + bdr);
		println("   ROUND_UP bdr =  " + bdr.setScale(2, BigDecimal.ROUND_UP));
		bdr = new BigDecimal("-0.333");
		print("bdr = " + bdr);
		println("   ROUND_UP bdr = " + bdr.setScale(2, BigDecimal.ROUND_UP));

		// ROUND_FLOOR: В меньшую сторону
		println();
		println("ROUND_FLOOR: В меньшую сторону");
		bdr = new BigDecimal("0.333");
		print("bdr =  " + bdr);
		println("   ROUND_FLOOR bdr =  " + bdr.setScale(2, BigDecimal.ROUND_FLOOR));
		bdr = new BigDecimal("-0.333");
		print("bdr = " + bdr);
		println("   ROUND_FLOOR bdr = " + bdr.setScale(2, BigDecimal.ROUND_FLOOR));

		// ROUND_HALF_UP: Округление вверх, если число после запятой >= .5
		println();
		println("ROUND_HALF_UP: Округление вверх, если число после запятой >= .5");
		bdr = new BigDecimal("0.335");
		print("bdr =  " + bdr);
		println("   ROUND_HALF_UP bdr =  " + bdr.setScale(2, BigDecimal.ROUND_HALF_UP));
		bdr = new BigDecimal("-0.335");
		print("bdr = " + bdr);
		println("   ROUND_HALF_UP bdr = " + bdr.setScale(2, BigDecimal.ROUND_HALF_UP));
		bdr = new BigDecimal("0.333");
		print("bdr =  " + bdr);
		println("   ROUND_HALF_UP bdr =  " + bdr.setScale(2, BigDecimal.ROUND_HALF_UP));
		bdr = new BigDecimal("-0.333");
		print("bdr = " + bdr);
		println("   ROUND_HALF_UP bdr = " + bdr.setScale(2, BigDecimal.ROUND_HALF_UP));

		// ROUND_HALF_DOWN: Округление вверх, если число после запятой > .5
		println();
		println("ROUND_HALF_DOWN: Округление вверх, если число после запятой > .5");
		bdr = new BigDecimal("0.335");
		print("bdr =  " + bdr);
		println("   ROUND_HALF_DOWN bdr =  " + bdr.setScale(2, BigDecimal.ROUND_HALF_DOWN));
		bdr = new BigDecimal("-0.335");
		print("bdr = " + bdr);
		println("   ROUND_HALF_DOWN bdr = " + bdr.setScale(2, BigDecimal.ROUND_HALF_DOWN));
		bdr = new BigDecimal("0.336");
		print("bdr =  " + bdr);
		println("   ROUND_HALF_DOWN bdr =  " + bdr.setScale(2, BigDecimal.ROUND_HALF_DOWN));
		bdr = new BigDecimal("-0.336");
		print("bdr = " + bdr);
		println("   ROUND_HALF_DOWN bdr = " + bdr.setScale(2, BigDecimal.ROUND_HALF_DOWN));

		// ROUND_HALF_EVEN: Округление по четности
		println();
		println("ROUND_HALF_EVEN: Округление по четности");
		bdr = new BigDecimal("0.335");
		print("bdr =  " + bdr);
		println("  ROUND_HALF_EVEN bdr =  " + bdr.setScale(2, BigDecimal.ROUND_HALF_EVEN));
		bdr = new BigDecimal("0.325");
		print("bdr =  " + bdr);
		println("  ROUND_HALF_EVEN bdr =  " + bdr.setScale(2, BigDecimal.ROUND_HALF_EVEN));

		bdr = new BigDecimal("-0.335");
		print("bdr = " + bdr);
		println("  ROUND_HALF_EVEN bdr = " + bdr.setScale(2, BigDecimal.ROUND_HALF_EVEN));
		bdr = new BigDecimal("-0.325");
		print("bdr = " + bdr);
		println("  ROUND_HALF_EVEN bdr = " + bdr.setScale(2, BigDecimal.ROUND_HALF_EVEN));

		// ROUND_UNNECESSARY: без округления
		println();
		println("ROUND_UNNECESSARY: без округления");
		bdr = new BigDecimal("0.333");
		print("bdr =  " + bdr);
		println("   ROUND_UNNECESSARY bdr =  " + bdr.setScale(3, BigDecimal.ROUND_UNNECESSARY));
		bdr = new BigDecimal("-0.333");
		print("bdr = " + bdr);
		println("   ROUND_UNNECESSARY bdr = " + bdr.setScale(3, BigDecimal.ROUND_UNNECESSARY));

		println("\nПример деления с округленим");
		BigDecimal bd01 = new BigDecimal("1");
		BigDecimal bd03 = new BigDecimal("3");
		// BigDecimal bd1d3 = bd01.divide(bd03); // вызовет ошибку ArithmeticException
		BigDecimal bd1d3 = bd01.divide(bd03, 5, BigDecimal.ROUND_HALF_UP);
		println("bd1d3 = " + bd1d3);

		println("\nПримеры сравнения");
		BigDecimal a = new BigDecimal("2.00");
		BigDecimal b = new BigDecimal("2.0");
		println("a = " + a + "  b = " + b);
		println("a.equals(b) = " + a.equals(b)); // ложь

		// возвращает (-1 если a < b), (0 если a == b), (1 если a > b)
		println("a.compareTo(b) = " + a.compareTo(b));
		// возвращает (-1 если a < 0), (0 если a == 0), (1 если a > 0)
		println("a.signum() = " + a.signum());

		// убираем незначащие нули в конце BigDecimal
		println("\nУбираем незначащие нули в конце BigDecimal");
		BigDecimal bd_1 = new BigDecimal("1.55");
		BigDecimal bd_2 = new BigDecimal("3.15");
		BigDecimal bd_3 = bd_1.add(bd_2);
		println("bd_3 = " + bd_3);
		println("bd_3 = " + bd_3.stripTrailingZeros());

	}

}
