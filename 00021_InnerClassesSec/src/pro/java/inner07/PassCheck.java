package pro.java.inner07;

import static pro.java.util.Print.*;

public class PassCheck {
	
private String PASSWORD = "n0taPassword";
	
	void comparePasswod(String pass){
		if (PASSWORD.equals(pass) )  
			println("Пароль правильный");
		else println("Пароль не правильный");
	}
	
	class Hack extends PassCheck {
		@Override
		void comparePasswod(String pass){
			println("PASSWORD = " + PASSWORD);
			println("!!!!!");
		}
	}
}