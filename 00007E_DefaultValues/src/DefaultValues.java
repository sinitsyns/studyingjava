import static pro.java.util.Print.*;
public class DefaultValues {
	//пример значений по умолчанию для примитивных типов
	static boolean defBoolean;
	static char defChar;
	static byte defByte;
	static short defShort;
	static int defInt;
	static long defLong;
	static float defFloat;
	static double defDouble;

	public static void main(String[] args) {
		//int localInt;
		//println("localInt="+localInt);
		
		println("defBoolean="+defBoolean);
		println("defChar="+defChar);
		println("defByte="+defByte);
		println("defShort="+defShort);
		println("defInt="+defInt);
		println("defLong="+defLong);
		println("defFloat="+defFloat);
		println("defDouble="+defDouble);
	}
}
