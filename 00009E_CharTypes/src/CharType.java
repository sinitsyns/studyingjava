import static pro.java.util.Print.*;
public class CharType {

	public static void main(String[] args) {
		char om='\u0950'; //символ омкары
		char tab1='\t'; //символ табуляции
		char tab2=9; //символ табуляции
		char tab3='\u0009'; //символ табуляции
		print(tab1);
		println(om);
		char ё='ё';
		print(tab2);
		print(ё);
		print(tab3);
		ё=1105; //десятичный код ё
		print(ё);
		print(tab1);
		ё='\u0451'; //шестнадцатиричный код ё
		println(ё);
		print('\t');
		println("\"Hello\n\t\t World!\"");
		++ё;
		print('\t');
		println(ё);

	}

}
