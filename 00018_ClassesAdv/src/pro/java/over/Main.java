package pro.java.over;

import static pro.java.util.Print.*;

class A {
	String a = "Class A";
	void prt() { println(a); }
	void prta() { println(a); }
}

class B extends A {
	String b = "Class B";
	void prt() { println(b); }
	void superprt() { super.prt(); }
}

class C extends B {
	String c = "Class C";
	void prt() { println(c); }
	void superprt() { super.prt(); }
}

public class Main {
	public static void main(String[] args) {
		C c = new C();
		c.prt();
		c.superprt();
		B b = c;
		b.prt();
		b.superprt();
		A a = b;
		a.prt();
		a.prta();
		b.prta();
		c.prta();
	}
}
