package pro.java.compostion;

import static pro.java.util.Print.*;

class MyString {
	String myStr;
	MyString(String s) { myStr =s; }
	public String toString() { return myStr; }
}

class MyClass {
	String str = "MyClass";
	MyString mystr = new MyString("MyString");
}

public class Main {
	public static void main(String[] args) {
		MyClass mc = new MyClass();
		println(mc.str);
		println(mc.mystr);
	}
}
