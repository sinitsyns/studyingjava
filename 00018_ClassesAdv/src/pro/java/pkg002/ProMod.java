package pro.java.pkg002;

import static pro.java.util.Print.println;

//модификатор доступа к классу по умолчанию
class ProMod {

	protected static int proInt = 1108;
	
	static void proPrtLine(){
		println("--- proPrtLine ---");
	}

}
