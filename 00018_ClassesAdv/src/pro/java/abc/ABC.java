package pro.java.abc;

import static pro.java.util.Print.*;

public class ABC {

	public static void main(String[] args) {

		A a = new A();
		B b = new B();
		C c = new C();
		a.printA();
		a.printX();
		printLnLineLn();
		b.printB();
		b.printX();
		printLnLineLn();
		c.printC();
		c.printX();
		printLnLineLn();
		println(a.x);
		println(b.x);
		println(c.x);
		printLnLineLn();
		println(((A)b).x);
		println(((A)c).x);
		println(((B)c).x);
		printLnLineLn();
		b.printA();
		c.printA();
		c.printB();
		println("**********");
		// пример присвоения переменной суперкласса
		// ссылки на объект подкласса
		A ab;
		ab = new B();
		ab.printA();
		//ab.printB(); // ОШИБКА!
		B bc = new C();
		bc.printA();
		bc.printB();
		bc.printX();
		// bc.printc(); // ОШИБКА!
	}

}
