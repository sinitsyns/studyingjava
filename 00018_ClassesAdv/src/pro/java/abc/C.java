package pro.java.abc;

import static pro.java.util.Print.*;

class C extends B {
	String x = "Класс C";

	void printC() {
		println("in method C");
		super.printX();
		printLnLineLn();
	}
	
	void printX(){
		print("X->" + x);
	}
}
