package pro.java.shapes02;

import static pro.java.util.Print.*;

class Shape {

	void drow() {
		println("Shape.drow()");
	}

	Shape() {
		println("Shape() перед вызовом drow()");
		drow();
		println("Shape() после вызова drow()");
	}
}

class Circle extends Shape {
	private String color;
	
	{
		color = "Красный";
	}

	Circle(String clr) {
		color = clr;
		println("Circle(), color = " + color);
	}

	void drow() {
		println("Circle.drow(), color = " + color);
	}

}

public class Main {
	public static void main(String[] args) {
		new Circle("Жёлтый");
	}
}
