package pro.java.animal;

class Dog extends Animal {

	Dog(String aType) {
		super(aType);
	}

	@Override
	void getSound() {
		System.out.println("Гав-гав");
	}

}
