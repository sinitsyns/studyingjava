package pro.java.animal;

class Cat extends Animal {

	Cat(String aType) {
		super(aType);
	}

	@Override
	void getSound() {
		System.out.println("Мяу-мяу");
	}

}
