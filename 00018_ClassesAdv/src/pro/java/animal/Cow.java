package pro.java.animal;

class Cow extends Animal {
	
	Cow(String aType) {
		super(aType);
	}

	@Override
	void getSound() {
		System.out.println("Му-му");
	}

}
