package pro.java.construct;
import static pro.java.util.Print.*;

public class B extends A {

	String b;
	{
		println("Инициализационный блок класса B");
	}

	B() {
		b = "Класс B";
		println(b + " Конструктор");
	}

}
