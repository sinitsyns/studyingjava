package pro.java.construct;
import static pro.java.util.Print.*;

public class A {

	String a;
	{ 
		println("Инициализационный блок класса А");
	}
	
	A (){
		a = "Класс A";
		println(a + " Конструктор");
	}

}
