package pro.java.construct;
import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {
		// Примеры вызова конструкторов суперклассов
		A a = new A();
		println();
		B b = new B();
		println();
		C c = new C();

	}

}
