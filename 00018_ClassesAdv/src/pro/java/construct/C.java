package pro.java.construct;
import static pro.java.util.Print.println;

public class C extends B{
	
String c;
	{ 
		println("Инициализационный блок класса C");
	}
	
	C (){
		c = "Класс C";
		println(c + " Конструктор");
	}

}
