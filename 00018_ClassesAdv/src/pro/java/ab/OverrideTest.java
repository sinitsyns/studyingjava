package pro.java.ab;

import static pro.java.util.Print.*;

public class OverrideTest {

	public static void main(String[] args) {
		// Пример переопределения статических и обычных методов
		// и затенения полей

		B b = new B();  // Создать новый объект типа B
		println(b.i);   // Ссылается на B.i; выводит 2
		println(b.j);   // Ссылается на B.j; выводит 2.0
		println(b.f()); // Ссылается на B.f(); выводит -2
		println(b.g()); // Ссылается на B.g(); выводит B
		println(B.g()); // Лучше вызывать B.g()

		A a = (A) b;    // Привести b к экземпляру класса A
		println(a.i);   // Теперь ссылается на A.i; выводит 1
		println(a.j);   // Теперь ссылается на A.j; выводит 1.0
		println(a.f()); // Ссылается на B.f(); выводит -2
		println(a.g()); // Ссылается на A.g(); выводит A
		println(A.g()); // Лучше вызывать A.g()

	}

}
