import static pro.java.util.Print.*;

public class IntegralTypes02 {

	public static void main(String[] args) {
		// Пример простых побитовых операций в Java
		println("Пример побитового НЕ (~)");
		byte b1 = 0b0000_1100;
		// если раскомментировать строку ниже, то компилятор выдаст ошибку
		// приведения типов
		// byte b2=~b1;
		byte b2 = (byte) ~b1; // а вот с приведением к типу byte уже все хорошо

		println("b1 как десятичное = " + b1);
		println("b2 как десятичное = " + b2);
		print("b1 как двоичное = ");
		printlnByte(b1);
		print("b2 как двоичное = ");
		printlnByte(b2);
		println("\nПример побитового И (&)");
		b1 = 10;
		b2 = 7;
		println("b1 как десятичное = " + b1);
		println("b2 как десятичное = " + b2);
		print("b1 как двоичное = ");
		printlnByte(b1);
		print("b2 как двоичное = ");
		printlnByte(b2);
		byte b3 =(byte)(b1&b2);
		print("b3 как двоичное = ");
		printlnByte(b3);
		println("b3 как десятичное = " + b3);
		println("\nПример побитового ИЛИ (|)");
		println("b1 как десятичное = " + b1);
		println("b2 как десятичное = " + b2);
		print("b1 как двоичное = ");
		printlnByte(b1);
		print("b2 как двоичное = ");
		printlnByte(b2);
		b3 =(byte)(b1|b2);
		print("b3 как двоичное = ");
		printlnByte(b3);
		println("b3 как десятичное = " + b3);
		println("\nПример побитового исключающего ИЛИ (^)");
		println("b1 как десятичное = " + b1);
		println("b2 как десятичное = " + b2);
		print("b1 как двоичное = ");
		printlnByte(b1);
		print("b2 как двоичное = ");
		printlnByte(b2);
		b3 =(byte)(b1^b2);
		print("b3 как двоичное = ");
		printlnByte(b3);
		println("b3 как десятичное = " + b3);
		println("\nПример побитового сдвига влево (<<)");
		b1=7;
		b2=3;
		println("b1 как десятичное = " + b1);
		println("b2 сдвигает на " + b2+ " позиции влево");
		print("b1 как двоичное = ");
		printlnByte(b1);
		b3 =(byte)(b1<<b2);
		print("b3 как двоичное = ");
		printlnByte(b3);
		println("b3 как десятичное = " + b3);
		println("\nПример побитового сдвига вправо со знаком (>>)");
		b1=27;
		println("b1 как десятичное = " + b1);
		println("b2 сдвигает на " + b2+ " позиции вправо");
		print("b1 как двоичное = ");
		printlnByte(b1);
		b3 =(byte)(b1>>b2);
		print("b3 как двоичное = ");
		printlnByte(b3);
		println("b3 как десятичное = " + b3);
		println();
		b1=-50;
		b2=2;
		println("b1 как десятичное = " + b1);
		println("b2 сдвигает на " + b2+ " позиции вправо");
		print("b1 как двоичное = ");
		printlnByte(b1);
		b3 =(byte)(b1>>b2);
		print("b3 как двоичное = ");
		printlnByte(b3);
		println("b3 как десятичное = " + b3);
	}
}
