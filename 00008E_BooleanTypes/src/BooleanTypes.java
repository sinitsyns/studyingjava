import static pro.java.util.Print.*;
public class BooleanTypes {

	public static void main(String[] args) {
		// примеры работы с примитивным типом boolean
		boolean a=true;
		boolean b=false;
		println("a="+a+"   b="+b);
		println("a|b="+(a|b));
		println("a&b="+(a&b));
		println("a^b="+(a^b));
		println("!a="+!a);
		println("now a="+a+"   b="+b);
		println("after b|=a b="+(b|=a)); //b=b|a
		println("now a="+a+"   b="+b);
		println("after a^=b a="+(a^=b)); //a=a^b
		println("now a="+a+"   b="+b);
		println("after b&=a b="+(b&=a)); //b=b&a
		println("now a="+a+"   b="+b);
		
		int x=5;
		int y=3;
		int z=5;
		println();
		println("x="+x+"   y="+y+"   z="+z);
		println("x>y "+(x>y));
		println("x<y "+(x<y));
		println("x!=z "+(x!=z));
		println("x==z "+(x==z));
		println("x>=z "+(x>=z));
		println("y<=x "+(y<=x));
		
	}
}
