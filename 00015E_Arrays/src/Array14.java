import static pro.java.util.Numbers.*;
import static pro.java.util.Print.*;

public class Array14 {

	public static void main(String[] args) {
		// Пример сортировки двумерного массива с переменной длиной строк

		// генерируем двумерный массив с переменной длиной строк
		int x = randomInRange(5, 10); // количество строк
		int[][] iOrigin = new int[x][]; // создали массив из x строк

		// генерируем разную длину строк
		for (int i = 0; i < x; ++i)
			iOrigin[i] = new int[randomInRange(1, 10)];

		println("\nНе сортированный массив");

		// заполняем массив случайными числами и выводим его на косоль
		for (int i = 0; i < x; ++i) {
			for (int j = 0; j < iOrigin[i].length; ++j) {
				iOrigin[i][j] = randomInRange(10, 99);
				print(" " + iOrigin[i][j]);
			}
			println();
		}

		// сортируем двумерный массив со строками переменной длины
		int tmp = 0;
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < iOrigin[i].length; j++) {
				for (int m = 0; m < x; m++) {
					for (int n = 0; n < iOrigin[m].length; n++) {
						if (iOrigin[m][n] > iOrigin[i][j]) {
							tmp = iOrigin[m][n];
							iOrigin[m][n] = iOrigin[i][j];
							iOrigin[i][j] = tmp;
						}
					}
				}
			}
		}

		println("\nОтсортированный массив");
		for (int i = 0; i < x; ++i) {
			for (int j = 0; j < iOrigin[i].length; ++j) {
				print(" " + iOrigin[i][j]);
			}
			println();
		}
	}
}
