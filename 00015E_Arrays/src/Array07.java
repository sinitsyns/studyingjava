import static pro.java.util.Print.*;
import java.util.Arrays;

public class Array07 {

	public static void main(String[] args) {
		// Примеры многомерных массивов Java

		int[] oneD = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		println("oneD: " + oneD);
		int[][] twoD = new int[3][];
		twoD[0] = oneD; //магия тут :)
		println("twoD: " + twoD);
		println("twoD[0]: " + twoD[0]);
		println("oneD:" + Arrays.toString(oneD));
		println("twoD:" + Arrays.deepToString(twoD));
		twoD[0][0] = 108;
		println("\ntwoD[0][0] = " + twoD[0][0]);
		println("oneD[0] = " + oneD[0]);
		println("\noneD:" + Arrays.toString(oneD));
		println("twoD:" + Arrays.deepToString(twoD));
		println("---------------------------------------------------\n");
		twoD[1] = new int[10];
		for (int i = 0; i < 10; ++i)
			twoD[1][i] = i + 10;
		println("twoD:" + Arrays.deepToString(twoD));
		oneD=twoD[1]; // и тут тоже магия :)
		println("oneD:" + Arrays.toString(oneD));
		println("\noneD: " + oneD);
		println("twoD[0]: " + twoD[0]);
		println("twoD[1]: " + twoD[1]);
	}
}
