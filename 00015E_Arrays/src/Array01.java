import static pro.java.util.Print.*;

public class Array01 {

	public static void main(String[] args) {
		// пример использования массива нулевой длины
		
		println("args.length = " + args.length + '\n');

		// классика жанра
		for (int i = 0; i < args.length; ++i) {
			println("args[" + i + "] = " + args[i]);
		}

		println();

		// извращение, но делает то же, что и классика
		for (int i = args.length; args.length > (args.length - i);) {
			println("args[" + (args.length - i) + "] = "
					+ args[args.length - i--]);
		}
	}
}
