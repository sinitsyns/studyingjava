import static pro.java.util.Numbers.*;
import static pro.java.util.Print.*;

public class Array02 {

	public static void main(String[] args) {
		// примеры одномерных массивов

		int[] intArray = new int[10];

		// заполняем массив случайными числами
		for (int i = 0; i < intArray.length; ++i) {
			intArray[i] = randomInRange(1, 100);
		}

		print("Массив intArray до сортировки: ");
		for (int x : intArray) {
			print(" " + x);
		}

		//сортировка массива по возрастанию пузырьком
		int tmp;
		for (int i = 0; i < intArray.length; i++) {
			for (int j = 0; j < intArray.length - i - 1; j++) {
				if (intArray[j] > intArray[j + 1]) {
					tmp = intArray[j];
					intArray[j] = intArray[j + 1];
					intArray[j + 1] = tmp;
				}
			}
		}

		print("\nМассив intArray после сортировки: ");
		for (int x : intArray) {
			print(" " + x);
		}
	}
}
