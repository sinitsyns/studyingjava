import static pro.java.util.Print.*;

public class Array06 {

	public static void main(String[] args) {
		// Примеры многомерных массиво Java

		println("Шаг 1");
		int[][] multiplicationTable = new int[10][];
		println("multiplicationTable = " + multiplicationTable + '\n');

		println("Шаг 2");
		for (int i = 0; i < 10; i++)
			println("multiplicationTable[" + i + "] = "
					+ multiplicationTable[i]);

		println("\nШаг 3");
		for (int i = 0; i < 10; i++) {
			multiplicationTable[i] = new int[10]; // создаем 10 массивов int
			println("multiplicationTable[" + i + "] = "
					+ multiplicationTable[i]);
		}

		println("\nСодержимое массива после создания");
		for (int i = 0; i < 10; ++i) {
			for (int j = 0; j < 10; ++j) {
				print(" " + multiplicationTable[i][j]);
			}
			println();
		}

		println("\nСодержимое массива после присвоения значений");
		for (int i = 0; i < 10; ++i) {
			for (int j = 0; j < 10; ++j) {
				multiplicationTable[i][j] = i * j;
				print(" " + multiplicationTable[i][j]);
			}
			println();
		}
	}
}
