import static pro.java.util.Print.*;

public class Operators03For {

	public static void main(String[] args) {
		// примеры оператора for
		int i; // i объявили вне цикла
		for (i = 0; i < args.length; ++i) {
			println("Сейчас i = " + i);
			println("Привет " + args[i]);
		}
		println("i = " + i); // i доступна вне цикла

		// j объявлена в цикле
		for (int j = 0; j < args.length; ++j) {
			println("Сейчас j = " + j);
			println("Hello " + args[j]);
		}
		// println("j = " + j); // j не доступна вне цикла

		int a, b;
		for (a = 1, b = 4; a < b; a++, b--) {
			println("\nНачало итерации");
			println("a = " + a);
			println("b = " + b);
			println("Завершение итерации");
		}
	}
}
