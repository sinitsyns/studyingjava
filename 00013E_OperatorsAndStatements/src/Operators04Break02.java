import static pro.java.util.Print.*;
public class Operators04Break02 {

	public static void main(String[] args) {
		// пример оператора break
		
		println("Начало программы");

		блок01: {
			println("Начало блока 01");
			блок02: {
				println("Начало блока 02");
				блок03: {
					println("Начало блока 03");

					switch (args.length) {
					case 0:
						break;
					case 1:
						break блок01;
					case 2:
						break блок02;
					case 3:
						break блок03;
					}
					println("Конец блока 03");
				}
				println("Конец блока 02");
			}
			println("Конец блока 01");
		}
		println("Конец программы");
	}
}
