import static pro.java.util.Print.*;

public class Operators04ReturnMethod {

	public static void vReturn(int i) {
		if (i > 5)
			return;
		println("Сейчас в методе vReturn. i=" + i);
	}

	public static String strReturn(int i) {

		if (i >= 5)
			return i + ">=5";
		return i + "<5";
	}
}
