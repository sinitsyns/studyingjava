import static pro.java.util.Print.*;

public class Operators04Continue {

	public static void main(String[] args) {
		// пример оператора continue

		for (int i = 1; i < 11; i++) {
			if (i % 2 != 0) continue;
			println("Число " + i + " четное");
		}
		
		println();

		outer: for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (j > i) {
					println();
					continue outer;
				}
				print("*");
			}
		}
	}
}
