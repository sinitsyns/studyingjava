import static pro.java.util.Print.*;

public class Operators03ForAdv1 {

	public static void main(String[] args) {
		// пример продвинутого оператора for

		for (int i = 0; (args.length > 0 && args.length > i); println("Итерация "
				+ i + " завершена")) {
			println("\nПривет " + args[i++]);
		}
	}
}
