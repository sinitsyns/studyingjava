import static pro.java.util.Print.println;

public class Operators02Switch {

	public static void main(String[] args) {
		// пример оператора switch

		switch (args.length) {
		case 0: {
			println("В командной строке нет аргументов");
			break;
		}
		case 1: {
			println("В командной строке 1 аргумент");
			break;
		}
		case 2: {
			println("В командной строке 2 аргумента");
			break;
		}
		case 3: {
			println("В командной строке 3 аргумента");
			break;
		}
		default:
			println("В командной строке больше 3 аргументов");
			break;
		}

		if (args.length > 0) {

			switch (args[0]) {
			case "Y":
			case "Yes":
			case "y": {
				println("Да!");
				break;
			}
			case "N":
			case "No":
			case "n": {
				println("Нет!");
				break;
			}
			default:
				 throw
				 new IllegalArgumentException("Первый аргумент должен быть Y или N");
			}

		}
	}
}
