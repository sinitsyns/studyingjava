import static pro.java.util.Print.*;

public class Operators02IfElse {

	public static void main(String[] args) {
		// Примеры оператора if/else

		if (args.length == 0)
			println("Нет аргументов в командной строке");

		if (args.length > 0)
			println("В командной строке есть аргументы");
		else
			println("В командной строке нет аргументов");

		if (args.length == 1)
			println("В командной строке 1 аргумент");
		else if (args.length == 2)
			println("В командной строке 2 аргумента");
		else if (args.length == 3)
			println("В командной строке 3 аргумента");
		else if (args.length > 3)
			println("В командной строке больше 3 аргументов");
	}
}
