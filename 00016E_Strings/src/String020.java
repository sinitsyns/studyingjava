import static pro.java.util.Print.*;
import static pro.java.util.Numbers.*;

public class String020 {

	public static void main(String[] args) {
		// примеры форматирования строк

		double[][] db = new double[10][10];

		// заполнение случайными числами и вывод неформатированных значений
		for (int i = 0; i < 10; ++i) {
			for (int j = 0; j < 10; ++j) {
				db[i][j] = randomInRange(1.0, 999.0);
				print(" " + db[i][j]);

			}
			println();
		}

		println();
		// вывод форматированных значений
		for (int i = 0; i < 10; ++i) {
			for (int j = 0; j < 10; ++j) {
				printf("%7.2f", db[i][j]);

			}
			println();
		}

	}

}
