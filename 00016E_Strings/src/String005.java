import static pro.java.util.Print.*;

public class String005 {

	public static void main(String[] args) {
		// пример работы с кодовыми точками и строками в Java

		// суррогатная пара математического символа Z
		char cz1 = '\uD835';
		char cz2 = '\uDD6B';

		println("cz1 = " + cz1);
		println("cz2 = " + cz2);

		// создали массив из одной кодовой точки
		int[] codePoint = { Character.toCodePoint(cz1, cz2) };
		println("\ncodePoint[0] = " + codePoint[0]);
		// преобразовали кодовую точку в символьную строку
		String zs = new String(codePoint, 0, 1);
		println("zs = " + zs);

		// создаем массив кодовых точек с кодами символов планет
		int[] planets = new int[9];
		for (int ip = 9791, i = 0; i < 9; ++i, ++ip)
			planets[i] = ip;

		// преобразовали массив кодовых точек планет в строку
		String sPlanets = new String(planets, 0, 9);
		println("\nsPlanets = " + sPlanets);

	}

}
