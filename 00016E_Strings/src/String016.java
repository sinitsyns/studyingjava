import static pro.java.util.Print.*;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class String016 {

	public static void main(String[] args) {
		// примеры некоторых методов класса String

		char[] chr = new char[10];
		byte[] bte = new byte[10];

		String str0 = null;
		String str1 = "";
		String str2 = "abc";
		String str3 = "Привет!";
		String str4 = "Это строка";
		String str5 = "Россия";
		String str6 = "строки";
		String str7 = " пробелы кругом ";

		// проверка на пустую строку
		// println("str0 is empty? " + str0.isEmpty()); // ошибка!
		println("str1 is empty? " + str1.isEmpty());
		println("str2 is empty? " + str2.isEmpty());

		// извлечение символа из строки
		println("символ по индексу 6 в str3 = " + str3.charAt(6));
		// извлечение символов из строки в массив символов
		println("str4.length() = " + str4.length());
		str4.getChars(4, 10, chr, 2);
		println("chr = " + Arrays.toString(chr));

		// извлечение байт из строки
		bte = str2.getBytes();
		println("bte (str2) = " + Arrays.toString(bte));

		try {
			// указали кодировку символов
			bte = str5.getBytes("Cp1251");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		println("bte (str5) = " + Arrays.toString(bte));

		// поиск подстроки в строке
		boolean b = str4.regionMatches(4, str6, 0, 5);
		println("поиск подстроки str6  в строке str4 " + b);

		// определение начинается ли строка с подстроки поиска
		b = str4.startsWith("Это");
		println("str4 начинается с Это " + b);
		b = str4.startsWith("ок", 7);
		println("str4 в позиции 7 начинается с ок " + b);

		// определение заканчивается ли строка с подстрокой поиска
		b = str4.endsWith("ока");
		println("строка 4 заканчивается на ока " + b);

		// поиск первого вхождения символа или подстроки в строке
		println("Первое вхождение символа т в str4 " + str4.indexOf(1090));
		println("Первое вхождение символа т в str4 " + str4.indexOf(1090, 2));
		println("Первое вхождение символа т в str4 " + str4.indexOf("т"));
		println("Первое вхождение символа т в str4 " + str4.indexOf("т", 2));

		// поиск последнего вхождения символа или подстроки в строке
		println("Последнее вхождение символа о в str4 " + str4.lastIndexOf(1086));
		println("Последнее вхождение символа о в str4 "
				+ str4.lastIndexOf(1086, 6));
		println("Последнее вхождение символа о в str4 " + str4.lastIndexOf("о"));
		println("Последнее вхождение символа о в str4 "
				+ str4.lastIndexOf("о", 6));

		// замена символов о на символы а в str4
		println("замена о на а в str4 = " + str4.replace("о", "а"));

		// определение содержит ли строка подстроку
		println("str6 содержит \"ок\" " + str6.contains("ок"));

		// замена подстроки в строке
		println("замена ок на ойк в str4 " + str4.replace("ок", "ойк"));

		// объединение строк со знаком разделителя
		println(String.join("-", "Java", "это", "круто!"));

		// изменение регистра букв в строке
		println("str6.toUpperCase() " + str6.toUpperCase());
		println("str3.toLowerCase() " + str3.toLowerCase());

		// убираем пробелы в начале и конце строки
		println("-" + str7.trim() + "-");

	}

}
