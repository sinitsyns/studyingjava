import static pro.java.util.Print.*;

public class String018 {

	public static void main(String[] args) {
		// примеры форматирования строк

		double x = 10000.0 / 3.0;

		println("Строка без форматирования x=" + x);
		printf("Строка с форматированием x=%.2f", x);
		println();
		printf("Строка с форматированием x=%8.2f", x);
		println();
		printf("Строка с форматированием x=%16.2f", x);
		println();
		String str = String.format("Строка с форматированием x=%10.2f", x);
		println(str);

	}
}
