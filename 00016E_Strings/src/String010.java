import static pro.java.util.Print.*;

public class String010 {

	public static void main(String[] args) {
		// примеры создания строк класса StringBuilder

		StringBuilder strBld1 = new StringBuilder("Это большая строка");
		String str1 = "не";
		strBld1.insert(4, str1);
		println("strBld1 = "+strBld1);
		str1 = null;
		strBld1.insert(strBld1.length(), " ");
		strBld1.insert(strBld1.length(), str1);
		println("strBld1 = "+strBld1);
		String009 str009 = new String009("!!! ");
		strBld1.insert(0, str009);
		println("strBld1 = "+strBld1);
		
		strBld1.delete(25, 108);
		println("strBld1 = "+strBld1);
		strBld1.deleteCharAt(0);
		println("strBld1 = "+strBld1);
		
		strBld1.replace(0, 2, "**");
		println("strBld1 = "+strBld1);
		
		println("reverse = " +strBld1.reverse());
		
		println("index ** = " + strBld1.indexOf("**"));
		println("index !! = " + strBld1.indexOf("!!"));
	}
}
